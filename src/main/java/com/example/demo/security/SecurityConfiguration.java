package com.example.demo.security;


//import javax.servlet.http.HttpServletResponse;

//import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.web.cors.CorsConfiguration;
//import org.springframework.web.cors.CorsConfigurationSource;
//import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	private static final String ADMIN = "ADMIN";
	private static final String VALIDATOR = "VALIDATOR";
	//private static final String VALIDATOR = "VALIDATOR";


	@Autowired
	private UserDetailsService userDetailsService;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService);
	}
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(userDetailsService);
		authProvider.setPasswordEncoder(passwordEncoder());

		return authProvider;
	}


	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
				.httpBasic()
			.and()
				.authorizeRequests()
					.antMatchers(HttpMethod.GET,"/request/list").hasAnyRole(ADMIN, VALIDATOR)
					.antMatchers(HttpMethod.POST,"/request/add").hasRole(ADMIN)
					.antMatchers(HttpMethod.PUT,"/request/update/{id}").hasRole(ADMIN)
					.antMatchers(HttpMethod.DELETE,"/request/delete/{id}").hasRole(ADMIN)
					//.antMatchers("/request/list").permitAll()
			.and()
				.sessionManagement()
					.sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
					.sessionFixation().migrateSession()
			.and()
				.formLogin()
			.and()
				.cors()
			.and()
				.csrf().disable();

	}

/*	@Bean
	public PasswordEncoder getPasswordEncoder() {
		return NoOpPasswordEncoder.getInstance();
	}
*/	


/*
//secure cookies method
public String setCookie(HttpServletResponse response) {
 
	Cookie cookie = new Cookie();
	cookie.setMaxAge(null);	//sets expiration after one minute
	cookie.setSecure(true);
	cookie.setHttpOnly(true); //encryption for user’s session data
	}
*/

}