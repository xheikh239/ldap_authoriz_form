package com.example.demo.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "requests")
public class Request {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @NotNull
    @Column(name = "matricule")
    private long Matricule;
    
    @NotEmpty(message = "email cannot be empty")
    @Email(message = "Please enter a valid email address", regexp = "^(.+)@(.+)$")
    private String email;

    @Column(name = "Raison")
    @NotEmpty
    private String raison;
    
    @Column(name = "dateDebut")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dateDebut;
	
	@Column(name = "dateFin")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dateFin;

    @AssertTrue(message = "invalid date")
    private boolean isValidEndDate() {
      return dateFin != null && !dateFin.isBefore(dateDebut);
    }
}