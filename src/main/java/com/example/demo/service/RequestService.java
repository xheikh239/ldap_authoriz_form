package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import com.example.demo.model.Request;


public interface RequestService {

    Request saveRequest(Request request);
    
    void deleteRequestById(long id);
    
    Optional<Request> findRequestById(Long id);
    
    List <Request> getAllRequests();
    
    //Request getRequestById(long id);
}