package com.example.demo.service;


import java.util.List;
import java.util.Optional;

import com.example.demo.model.Request;
import com.example.demo.repository.RequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class RequestServiceImpl implements RequestService {

    @Autowired
    private RequestRepository requestRepository;

    @Override
    public Request saveRequest(Request request) {
       return requestRepository.save(request);
    }
    
    @Override
    public void deleteRequestById(long id) {
        requestRepository.deleteById(id);
    }

	@Override
	public Optional<Request> findRequestById(Long id) {
		return requestRepository.findById(id);
	}

    @Override
    public List <Request> getAllRequests() {
        return requestRepository.findAll();
    }

  /*  @Override
    public Request getRequestById(long id) {
        Optional < Request > optional = requestRepository.findById(id);
        Request request = null;
        if (optional.isPresent()) {
            request = optional.get();
        } else {
            throw new RuntimeException(" Request not found for id :: " + id);
        }
        return request;
    }*/

}