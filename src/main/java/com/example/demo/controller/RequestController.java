package com.example.demo.controller;

import java.util.List;

import javax.validation.Valid;

import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.model.Request;
import com.example.demo.service.RequestService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.BindingResult;



//@Slf4j
@RestController
@CrossOrigin(origins = "http://localhost:8080")
@RequestMapping({"/request"})
public class RequestController {

    @Autowired
    private RequestService requestService;

    // display list of requests
	@GetMapping("/list")
    public List<Request> getAllRequests() {
        return requestService.getAllRequests();
    }

    @GetMapping("/{id}")//find by id
	public Request getRequest(@PathVariable(value = "id") Long id) {
	    return requestService.findRequestById(id).orElseThrow(() -> new ResourceNotFoundException("the id " + id + " not found"));
	}

	@PostMapping("/add")//save
	public String createRequest(@Valid @RequestBody Request request, BindingResult bindingResult) {
    // save employee request to database
    if (bindingResult.hasErrors()) {
		return "request has error";
    }
    requestService.saveRequest(request);
    return "request_saved";
	}

	@PutMapping("/update/{id}") //update
	public String updateRequest(@PathVariable(value = "id") Long id, @RequestBody Request request) {
		return requestService.findRequestById(id).map(req -> {
			req.setMatricule(request.getMatricule());
            req.setEmail(request.getEmail());
            req.setRaison(request.getRaison());
			req.setDateDebut(request.getDateDebut());
            req.setDateFin(request.getDateFin());
            
			requestService.saveRequest(req);
			return "request updated";
		}).orElseThrow(() -> new ResourceNotFoundException("the id " + id + " not found"));
	}

    @DeleteMapping("/delete/{id}")
	public String deleteRequest(@PathVariable(value = "id") Long id) {
		return requestService.findRequestById(id).map(req -> {
			requestService.deleteRequestById(id);
			return "requests deleted";
		}).orElseThrow(() -> new ResourceNotFoundException("the id " + id + " not found"));
	}
}
