INSERT INTO user(id, is_active, password, roles,user_name) 
VALUES (1,1,'$2a$12$p3hwW5QZZ9Eif7i1l8vjCugPvEp92H9k36vrOICeq/xqCP24/FNM2','ROLE_VALIDATOR','validator'),
(2,1,'$2a$12$p3hwW5QZZ9Eif7i1l8vjCugPvEp92H9k36vrOICeq/xqCP24/FNM2','ROLE_ADMIN','admin'),
(3,1,'$2a$12$p3hwW5QZZ9Eif7i1l8vjCugPvEp92H9k36vrOICeq/xqCP24/FNM2','ROLE_ADMIN','92606'),
(4,0,'$2a$12$p3hwW5QZZ9Eif7i1l8vjCugPvEp92H9k36vrOICeq/xqCP24/FNM2','ROLE_ADMIN','admin1');

